# README #

NOTE: THIS IS A WORK IN PROGRESS, THERE ARE BUGS

Just a simple, NON-BLOATED win32 program, written in Python, for displaying desktop notifications when twitch.tv channels go live.

### Features (so far) ###

* Runs in the background, sitting in your system tray
* Add up to 30 channel names to a text file to query them for broadcasts
* Shows a notification balloon when a channel goes live (click to open stream page in your browser)
* Click the icon to repeat the last message
* Right click to see which channels are currently live, as well as a timestamp showing when they last went active/inactive (since the program started)
* THAT'S IT

### Setup ###

* Seems to only work with Python 2.x right now
* Check requirements.txt for a list of dependencies
* To quickly install dependencies, navigate to the program folder and use the command:
```
#!batch
pip install -r requirements.txt
```