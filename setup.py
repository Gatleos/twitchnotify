from distutils.core import setup

setup(name='twitchnotify',
      version='1.0',
      packages=['tnotify'],
      scripts=['twitchnotify.pyw'],
      data_files=[
          ('icons', ['icons/balloontip.ico', 'icons/disconnected.ico', 'icons/online.ico']),
          ('config', ['config/channels.txt']),
          ('.', ['requirements.txt'])
      ]
      )
