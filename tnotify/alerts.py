

last_alert = None
queued_alerts = []
_queue_frequency = 5.0
_queue_timer = _queue_frequency


def update_queue(elapsed_time):
    """Goes through stacked up alerts on a timer, so they don't
    all zip by at once"""
    global _queue_timer, last_alert
    _queue_timer += elapsed_time
    if len(queued_alerts) > 0 and _queue_timer >= _queue_frequency:
        _queue_timer -= _queue_frequency
        last_alert = queued_alerts[0]
        queued_alerts.pop(0)
        return last_alert is not None
    return False


def reset_timer():
    global _queue_timer, _queue_frequency
    _queue_timer = _queue_frequency
