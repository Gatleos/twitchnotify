import webbrowser
from functools import partial

max_channels = 30
channels = []
menu_items = []


class Channel:
    """Represents a twitch channel, and its current status"""
    def __init__(self, name, url):
        self.name = name
        self.active = False
        self.last_active_change = ''
        self.url = url


def open_stream_page(channel, connection):
    """Opens a stream's url; if the url is not present,
    tries to load it in by querying the channel"""
    if len(channel.url) == 0:
        try:
            channel.url = connection.get_channel(channel.name).url
        except IOError:
            print('ERROR: failed to query channel "{}"'.format(channel.name))
    if len(channel.url) > 0:
        webbrowser.open_new_tab(channel.url)


def load_channels(file_path, connection):
    try:
        with open(file_path) as f:
            content = f.readlines()
            i = 0
            for c in content:
                # Sanity check
                if i >= max_channels:
                    break
                c = c.strip()
                if c[0] == '#' or len(c) == 0:
                    continue
                url = ''
                try:
                    url = connection.get_channel(c).url
                except IOError:
                    print('ERROR: failed to query channel "{}"'.format(c))
                channels.append(Channel(c, url))
                menu_items.append((channels[-1].name, None, partial(open_stream_page, channels[-1], connection)))
                print('Loaded channel: {0}'.format(c))
                i += 1
    except IOError as e:
        print("I/O error({0}): {1}: {2}".format(e.errno, e.strerror, file_path))
