# original version of this demo available at http://www.itamarst.org/software/
import win32api
import win32con
import win32gui
import win32gui_struct


class Taskbar:
    FIRST_ID = 1023
    QUIT = 'QUIT'
    SPECIAL_ACTIONS = [QUIT]

    def __init__(self):
        self.hicon = None
        self.tooltip = ''
        # menu
        self._next_action_id = self.FIRST_ID
        self.menu_options = ()
        self.menu_actions_by_id = dict()
        self.visible = 0
        message_map = {
            win32con.WM_DESTROY: self.on_destroy,
            win32con.WM_COMMAND: self.command,
            win32con.WM_USER + 20: self.on_notify,
        }
        # Register the Window class.
        wc = win32gui.WNDCLASS()
        self.hinst = wc.hInstance = win32api.GetModuleHandle(None)
        wc.lpszClassName = "PythonTaskbarDemo"
        wc.style = win32con.CS_VREDRAW | win32con.CS_HREDRAW
        wc.hCursor = win32gui.LoadCursor(0, win32con.IDC_ARROW)
        wc.hbrBackground = win32con.COLOR_WINDOW
        wc.lpfnWndProc = message_map  # could also specify a wndproc.
        class_atom = win32gui.RegisterClass(wc)
        # Create the Window.
        style = win32con.WS_OVERLAPPED | win32con.WS_SYSMENU
        self.hwnd = win32gui.CreateWindow(class_atom, "Taskbar Demo", style,
                                          0, 0, win32con.CW_USEDEFAULT, win32con.CW_USEDEFAULT,
                                          0, 0, self.hinst, None)
        win32gui.UpdateWindow(self.hwnd)
        self.closed = False

    def set_icon(self, hicon, tooltip=None):
        self.hicon = hicon
        self.tooltip = tooltip

    def show(self):
        """Display the taskbar icon"""
        flags = win32gui.NIF_ICON | win32gui.NIF_MESSAGE
        if self.tooltip is not None:
            flags |= win32gui.NIF_TIP
            nid = (self.hwnd, 0, flags, win32con.WM_USER + 20, self.hicon, self.tooltip)
        else:
            nid = (self.hwnd, 0, flags, win32con.WM_USER + 20, self.hicon)
        if self.visible:
            self.hide()
        win32gui.Shell_NotifyIcon(win32gui.NIM_ADD, nid)
        self.visible = 1

    def hide(self):
        """Hide the taskbar icon"""
        if self.visible:
            nid = (self.hwnd, 0)
            win32gui.Shell_NotifyIcon(win32gui.NIM_DELETE, nid)
        self.visible = 0

    def populate_menu(self, menu_options):
        self.menu_options = menu_options
        self.menu_options = self.menu_options + [('Quit', None, self.QUIT), ]
        self.menu_options = self._add_ids_to_menu_options(list(self.menu_options))

    def on_destroy(self, hwnd, msg, wparam, lparam):
        self.hide()
        win32gui.PostQuitMessage(0)  # Terminate the app.
        self.closed = True

    def on_notify(self, hwnd, msg, wparam, lparam):
        if lparam == win32con.WM_LBUTTONUP:
            self.on_click()
        elif lparam == win32con.WM_LBUTTONDBLCLK:
            self.on_double_click()
        elif lparam == win32con.WM_RBUTTONUP:
            self.on_right_click()
        elif lparam == 1029:
            self.on_balloon_clicked()
        return 1

    def command(self, hwnd, msg, wparam, lparam):
        menu_id = win32gui.LOWORD(wparam)
        self.execute_menu_option(menu_id)

    def execute_menu_option(self, menu_id):
        try:
            menu_action = self.menu_actions_by_id[menu_id]
        except KeyError:
            return
        if menu_action == self.QUIT:
            win32gui.DestroyWindow(self.hwnd)
        else:
            menu_action()

    def show_menu(self):
        menu = win32gui.CreatePopupMenu()
        self.create_menu(menu, self.menu_options)

        pos = win32gui.GetCursorPos()
        win32gui.SetForegroundWindow(self.hwnd)
        win32gui.TrackPopupMenu(menu,
                                win32con.TPM_LEFTALIGN,
                                pos[0],
                                pos[1],
                                0,
                                self.hwnd,
                                None)
        win32gui.PostMessage(self.hwnd, win32con.WM_NULL, 0, 0)

    def create_menu(self, menu, menu_options):
        for option_text, option_icon, option_action, option_id in menu_options[::-1]:
            if option_id in self.menu_actions_by_id:
                item, extras = win32gui_struct.PackMENUITEMINFO(text=option_text,
                                                                hbmpItem=option_icon,
                                                                wID=option_id)
                win32gui.InsertMenuItem(menu, 0, 1, item)
            else:
                submenu = win32gui.CreatePopupMenu()
                self.create_menu(submenu, option_action)
                item, extras = win32gui_struct.PackMENUITEMINFO(text=option_text,
                                                                hbmpItem=option_icon,
                                                                hSubMenu=submenu)
                win32gui.InsertMenuItem(menu, 0, 1, item)

    @staticmethod
    def prep_menu_icon(icon):
        # First load the icon.
        ico_x = win32api.GetSystemMetrics(win32con.SM_CXSMICON)
        ico_y = win32api.GetSystemMetrics(win32con.SM_CYSMICON)
        hicon = win32gui.LoadImage(0, icon, win32con.IMAGE_ICON, ico_x, ico_y, win32con.LR_LOADFROMFILE)

        hdc_bitmap = win32gui.CreateCompatibleDC(0)
        hdc_screen = win32gui.GetDC(0)
        hbm = win32gui.CreateCompatibleBitmap(hdc_screen, ico_x, ico_y)
        hbm_old = win32gui.SelectObject(hdc_bitmap, hbm)
        # Fill the background.
        brush = win32gui.GetSysColorBrush(win32con.COLOR_MENU)
        win32gui.FillRect(hdc_bitmap, (0, 0, 16, 16), brush)
        # unclear if brush needs to be feed.  Best clue I can find is:
        # "GetSysColorBrush returns a cached brush instead of allocating a new
        # one." - implies no DeleteObject
        # draw the icon
        win32gui.DrawIconEx(hdc_bitmap, 0, 0, hicon, ico_x, ico_y, 0, 0, win32con.DI_NORMAL)
        win32gui.SelectObject(hdc_bitmap, hbm_old)
        win32gui.DeleteDC(hdc_bitmap)

        return hbm

    def _add_ids_to_menu_options(self, menu_options):
        result = []
        for menu_option in menu_options:
            option_text, option_icon, option_action = menu_option
            if callable(option_action) or option_action in self.SPECIAL_ACTIONS:
                self.menu_actions_by_id.update({self._next_action_id: option_action})
                result.append(menu_option + (self._next_action_id,))
            elif True:
                result.append((option_text,
                               option_icon,
                               self._add_ids_to_menu_options(option_action),
                               self._next_action_id))
            else:
                print('Unknown item', option_text, option_icon, option_action)
            self._next_action_id += 1
        return result

    def on_click(self):
        """Override in subclassess"""
        pass

    def on_double_click(self):
        """Override in subclassess"""
        pass

    def on_right_click(self):
        """Override in subclasses"""
        pass

    def on_balloon_clicked(self):
        """Override in subclasses"""
        pass
