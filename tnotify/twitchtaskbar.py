import datetime
from functools import partial
import threading
import sys
import os
import alerts
import channel
from taskbar import *

# for menu item timestamps
time_format = '%I:%M %p'
# add more icons here!
icon_names = ["icons/disconnected.ico", "icons/balloontip.ico"]
# this one is for icons that appear in the right-click menu
menu_icon_names = ["icons/online.ico"]


class TwitchTaskbar(Taskbar):
    def __init__(self, connection):
        Taskbar.__init__(self)
        self.icons = {}
        self.offline = False
        self.offline_status_change = False
        # in seconds
        self.check_frequency = 30.0
        self.check_timer = self.check_frequency
        self.conn = connection
        self.show()
        self._query_thread = None
        self.menu_changed = False

    def on_click(self):
        if alerts.last_alert is not None:
            self.display_balloon(alerts.last_alert[0], alerts.last_alert[1])

    def on_double_click(self):
        pass

    def on_right_click(self):
        self.show_menu()

    def on_balloon_clicked(self):
        if alerts.last_alert is not None:
            channel.open_stream_page(alerts.last_alert[2], self.conn)

    def display_balloon(self, title, msg):
        flags = win32gui.NIF_ICON | win32gui.NIF_MESSAGE | win32gui.NIF_INFO
        nid = (self.hwnd, 0, flags, win32con.WM_USER + 20, self.hicon, 'twitchnotify', msg, 5000, title,
               win32gui.NIIF_NONE)
        win32gui.Shell_NotifyIcon(win32gui.NIM_MODIFY, nid)

    def _query_channels(self):
        self.menu_changed = False
        for i in range(len(channel.channels)):
            ch = channel.channels[i]
            try:
                stream = self.conn.get_stream(ch.name)
                if self.offline:
                    self.offline_status_change = True
                self.offline = False
            except IOError:
                if not self.offline:
                    self.offline_status_change = True
                self.offline = True
                continue
            if stream is None:
                if ch.active:
                    ch.last_active_change = datetime.datetime.now().time().strftime(time_format)
                    channel.menu_items[i] = (
                        '{} - {}'.format(ch.name, ch.last_active_change), None,
                        partial(channel.open_stream_page, ch, self.conn))
                    self.menu_changed = True
                ch.active = False
            elif not ch.active:
                ch.active = True
                ch.last_active_change = datetime.datetime.now().time().strftime(time_format)
                alerts.last_channel = ch
                channel.menu_items[i] = (
                    '{} - {}'.format(ch.name, ch.last_active_change), self.icons['icons/online.ico'],
                    partial(channel.open_stream_page, ch, self.conn))
                alerts.queued_alerts.append((stream.channel.displayname, stream.channel.status, ch))
                self.menu_changed = True
                alerts.queue_timer = 0.0
        if self.offline_status_change:
            alerts.reset_timer()
            self.offline_status_change = False
            if self.offline:
                self.set_icon(self.icons["icons/disconnected.ico"], "twitchnotify (can't connect)")
            else:
                self.set_icon(self.icons["icons/balloontip.ico"], 'twitchnotify')
            self.show()

    def check_channels(self, elapsed):
        self.check_timer += elapsed
        if self.check_timer >= self.check_frequency:
            self.check_timer -= self.check_frequency
            if self._query_thread is None or not self._query_thread.is_alive():
                self._query_thread = threading.Thread(target=self._query_channels)
                self._query_thread.start()
        return self.menu_changed

    def load_icons(self):
        icon_flags = win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE
        for i in range(len(icon_names)):
            try:
                icon_path = os.path.abspath(os.path.join(sys.path[0], icon_names[i]))
                self.icons[icon_names[i]] = win32gui.LoadImage(self.hinst, icon_path, win32con.IMAGE_ICON, 0, 0, icon_flags)
            except win32gui.error as e:
                print(e)
                self.icons[icon_names[i]] = win32gui.LoadIcon(0, win32con.IDI_APPLICATION)
        for i in range(len(menu_icon_names)):
            icon_path = os.path.abspath(os.path.join(sys.path[0], menu_icon_names[i]))
            self.icons[menu_icon_names[i]] = Taskbar.prep_menu_icon(icon_path)
        print('Icons loaded')
