if __name__ == '__main__':
    import time
    import pytwitcherapi
    from tnotify.taskbar import *
    from tnotify.twitchtaskbar import *
    from tnotify.channel import *

    file_path = 'config/channels.txt'

    # initialize icon data and establish a connection
    conn = pytwitcherapi.TwitchSession()
    # load the channels in, creating a menu for them
    channel.load_channels(file_path, conn)
    # everything is ready, show our icon
    t = TwitchTaskbar(conn)
    t.populate_menu(channel.menu_items)
    t.load_icons()
    t.set_icon(t.icons['icons/balloontip.ico'])
    t.show()

    start = 0.0
    elapsed = 0.0

    # main loop
    while True:
        # calculate timer
        elapsed = time.clock() - start
        start = time.clock()
        # any change in online status for one of the channels?
        if t.check_channels(elapsed):
            t.populate_menu(channel.menu_items)
        # is it time to display the next message?
        if alerts.update_queue(elapsed):
            t.display_balloon(alerts.last_alert[0], alerts.last_alert[1])
        # get input since the last iteration
        win32gui.PumpWaitingMessages()
        if t.closed:
            break
        # sleep to finish loop
        time.sleep(max(0.016 - elapsed, 0.0))
    print('Bye!')
